**Welcome to the public repository for the addtional content of a paper we submitted to SPLC 2021**

**"Safety, Security, and Configurable Software Systems: A Systematic Mapping Study"**

This repository provides additional information to the conducted mapping study:

* SPLC2021-scopus-full-list.csv - Contains information about the 367 paper that we have extracted from Scopus
